{
  inputs.flake-utils.url = "github:numtide/flake-utils";
  outputs = { self, nixpkgs, flake-utils }:
    let overlay = import ./overlay.nix;
    in flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ overlay ];
        };
      in {
        packages = { inherit (pkgs) financier-dist; };
        defaultPackage = self.packages.${system}.financier-dist;
      }) // {
        inherit overlay;
        nixosModule = import ./module.nix;
      };
}
