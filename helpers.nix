{
  fetchFinancier = { fetchgit, meta ? import ./meta.nix }:
    fetchgit { inherit (meta) url rev sha256; };
}
