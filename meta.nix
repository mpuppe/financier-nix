rec {
  version = "1.7.3";
  nodeVersion = "12";

  # Arguments for fetchgit
  url = "https://gitlab.com/mpuppe/financier.git";
  rev = "83367cae265af4a036a05766c6f80ea22d1cc1ec";
  sha256 = "0h7222j9fk4b3fkc7c6gbhl0xranpl5swapkwibb0aba0g6fykfg";
}
