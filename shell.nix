{ pkgs ? import <nixpkgs> { } }:
let
  meta = import ./meta.nix;
  src = (import ./helpers.nix).fetchFinancier {
    inherit (pkgs) fetchgit;
    inherit meta;
  };
in pkgs.mkShell {
  shellHook = ''
    export FINANCIER_SRC="${src}"
    export FINANCIER_NODE_VERSION="${meta.nodeVersion}"
  '';
  buildInputs = with pkgs; [ nodePackages.node2nix ];
}
