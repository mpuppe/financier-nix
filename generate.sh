node2nix "-$FINCANCIER_NODE_VERSION" --development \
	--input "$FINANCIER_SRC/package.json" \
	--lock "$FINANCIER_SRC/package-lock.json" \
	--supplement-input supplement.json \
	--composition node2nix.nix

# Fix the src attribute for the financier package
pattern='\(\s*\).*'$(echo $FINANCIER_SRC | sed 's/\//\\\//g')'.*'
sed -i "s/$pattern/\1src = (import .\/helpers.nix).fetchFinancier { inherit fetchgit; };/" node-packages.nix