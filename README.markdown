**IMPORTANT**: This repository has been migrated to https://codeberg.org/puppe/financier-nix

# Nix support for financier

This repository contains files for building
[financier](https://gitlab.com/mpuppe/financier/) with the
[Nix](https://nixos.org/manual/nix/stable/) package manager.

First, you should update the necessary Nix files:

``` bash
nix-shell --run 'bash generate.sh'
```

Afterwards, the distribution for financier can be built:

``` bash
nix-build dist.nix
```

There is also a module (module.nix) that you can use in your NixOS
configuration.
