{ pkgs ? import <nixpkgs> { inherit system; }, system ? builtins.currentSystem
}:

let
  financier = (import ./default.nix { inherit pkgs system; }).package;
  meta = import ./meta.nix;

in pkgs.stdenv.mkDerivation {
  inherit (meta) version;
  pname = "financier-dist";
  src = "${financier}/lib/node_modules/financier";
  buildInputs = [ financier.nodejs ];
  phases = [ "unpackPhase" "buildPhase" "installPhase" ];
  buildPhase = ''
    env NO_UPDATE_NOTIFIER=true npm run build
  '';
  installPhase = ''
    cp -r dist $out/
  '';
}
