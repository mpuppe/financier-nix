{ config, lib, pkgs, system, ... }:
with lib;
let
  inherit (pkgs) financier-dist;
  cfg = config.services.financier;
in {
  imports = [ ];

  options.services.financier = {
    enable = mkEnableOption "Financier";

    hostName = mkOption {
      type = types.str;
      description = ''
        Name for the virtual host for the web server.
      '';
      example = "example.org";
    };

    server = mkOption {
      type = types.enum [ "nginx" "caddy" ];
      description = ''
        The web server to be used for serving Financier. Either "nginx"
        or "caddy".
      '';
      default = "caddy";
      example = "example.org";
    };
  };

  config = mkIf cfg.enable {
    nixpkgs.overlays = [ (import ./overlay.nix) ];

    networking.firewall.allowedTCPPorts = [ 80 443 ];

    services.nginx = mkIf (cfg.server == "nginx") {
      enable = true;

      virtualHosts."${cfg.hostName}" = {
        forceSSL = true;
        enableACME = true;
        root = financier-dist;
        locations."/".tryFiles = "$uri $uri/ /index.html";
      };
    };

    services.caddy = mkIf (cfg.server == "caddy") {
      enable = true;

      config = ''
        ${cfg.hostName} {
          root * ${financier-dist}
          file_server
          try_files {path} {path}/ /index.html
        }
      '';
    };
  };
}
