{ pkgs ? import <nixpkgs> { inherit system; }, system ? builtins.currentSystem
}:

let derivations = import ./node2nix.nix { inherit pkgs system; };
in derivations // {
  package = derivations.package.overrideAttrs (oldAttrs: rec {
    # Puppeteer (a dependency for testing) would try to download Chromium
    # during installation. Downloading Chromium would not work though because
    # Nix builds are sandboxed and cannot download arbitrary things. We
    # therefore prevent Puppeteer from downloading anything.
    preInstallPhases = "skipChromiumDownload";
    skipChromiumDownload = ''
      export PUPPETEER_SKIP_DOWNLOAD=1
    '';
  });
}
